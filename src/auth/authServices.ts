import axios from 'axios';
import jwtDecode from "jwt-decode";

export function signIn(data: any) {
    return axios.post("/v2/sign-in", data).then((resp) => resp.data);
}

export function signOut() {
    setSession(null)
}

export function setSession(access_token: (string | null)) {
    if (access_token) {
        localStorage.setItem('jwt_access_token', access_token);
        axios.defaults.headers.common.Authorization = `Bearer ${access_token}`;
    } else {
        localStorage.removeItem('jwt_access_token');
        delete axios.defaults.headers.common.Authorization;
    }
};

export function isAuthTokenValid(access_token: string) {
    if (!access_token) {
        return false;
    }
    const decoded = jwtDecode(access_token);
    const currentTime = Date.now() / 1000;
    // @ts-ignore
    if (decoded.exp < currentTime) {
        console.warn('access token expired');
        return false;
    }

    return true;
};

export function getAccessToken():string {
    if (typeof window !== 'undefined') {
        return window.localStorage.getItem('jwt_access_token')??"";
    }
    return ""
};