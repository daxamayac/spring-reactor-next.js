import React, {memo, useEffect, useLayoutEffect, useState} from "react";
import {getAccessToken, isAuthTokenValid, setSession} from "./authServices";
import {useRouter} from "next/router";
import {NextPage} from "next";
import {CircularProgress} from "@mui/material";

interface authProviderProps {
    children?: | React.ReactElement | React.ReactElement[];
}

function AutProvider({children}: authProviderProps) {
    const [isLoading, setIsLoading] = useState(true);
    const router = useRouter();

    useEffect(() => {
        const access_token = getAccessToken();
        if (router.pathname === '/') {
            router.push("/sign-in");
            return;
        }
        if (router.pathname === '/sign-in') {
            setIsLoading(false);
            return;
        }

        if (access_token && isAuthTokenValid(access_token)) {
            setSession(access_token)
            setIsLoading(false);
            return;
        }
        if (!access_token) {
            router.push("/sign-in");
        }

    }, [router.pathname]);

    if (isLoading)
        return <div className="flex items-center justify-center h-screen w-screen">
            <CircularProgress variant="indeterminate" size={200}/>
        </div>

    return <>{children}</>;
};

export default memo(AutProvider);
