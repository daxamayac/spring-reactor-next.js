import React, {memo} from "react";
import AppBar from '@mui/material/AppBar';
import MuiToolbar from '@mui/material/Toolbar';
import clsx from 'clsx';
import {Divider, Drawer, IconButton, ListItem, ListItemButton, ListItemText} from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import MenuIcon from '@mui/icons-material/Menu';
import List from "@mui/material/List"
import navigationConfig from "../../configs/navigationConfig";
import {useRouter} from "next/router";

interface toolbarProps {
    className?: string;
    window?: () => Window;
}

const drawerWidth = 240;
// @ts-ignore
function Toolbar({className, window}: toolbarProps) {
    const router = useRouter()
    const shallow = false;
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const navigation = navigationConfig;
    const basePath = router.pathname;
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{textAlign: 'center'}}>
            <Typography variant="h6" sx={{my: 2}}>
                MUI
            </Typography>
            <Divider/>
            <List>
                {navigation.map((item) => (
                    <ListItem key={item.id}>
                        <ListItemButton selected={item.url.toLowerCase() === basePath} sx={{textAlign: 'center'}} onClick={()=> router.push(item.url, undefined, { shallow })}>
                            <ListItemText className="whitespace-nowrap" primary={item.title}/>
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
        </Box>
    );
    const container = window !== undefined ? () => window().document.body : undefined;
    return (
        <AppBar
            id="toolbar"
            className={clsx('flex relative z-20 shadow-md', className)}
            color="default"
            position="static"
        >
            <MuiToolbar className="p-0 min-h-64 mx-20">
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                    onClick={handleDrawerToggle}
                    sx={{mr: 2, display: {sm: 'none'}}}
                >
                    <MenuIcon/>
                </IconButton>
                <Typography
                    variant="h6"
                    component="div"
                    sx={{flexGrow: 1, display: {xs: 'none', sm: 'block'}}}
                >
                    Spring WebFlux & Next.js
                </Typography>

                <Box  sx={{display: {xs: 'none', sm: 'flex'}}}>
                    {navigation.map((item) =>
                        <ListItem key={item.id}>
                            <ListItemButton selected={item.url.toLowerCase() === basePath} sx={{textAlign: 'center'}} onClick={()=>router.push(item.url, undefined, { shallow })}>
                                <ListItemText className="whitespace-nowrap" primary={item.title}/>
                            </ListItemButton>
                        </ListItem>
                    )}
                </Box>
            </MuiToolbar>
            <Box component="nav">
                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: {xs: 'block', sm: 'none'},
                        '& .MuiDrawer-paper': {boxSizing: 'border-box', width: drawerWidth},
                    }}
                >
                    {drawer}
                </Drawer>
            </Box>
        </AppBar>
    );
}

export default memo(Toolbar);
