import React from "react";
import Footer from "./Footer";
import Toolbar from "./Toolbar";
import DaxDialog from "../DaxDialog";
import {useRouter} from "next/router";


interface LayoutProps {
    children?: | React.ReactElement | React.ReactElement[];
}

function Layout({children}: LayoutProps) {
    const router = useRouter();
    const isSignIn = router.pathname === "/sign-in" || router.pathname === "/404";

    return <div id="layout" className="w-full flex">
        <DaxDialog/>
        <div className="flex flex-auto min-w-0">
            <main id="main" className="flex flex-col flex-auto min-h-full min-w-0 relative z-10">
                {!isSignIn && <Toolbar/>}
                <div className="flex flex-col flex-auto min-h-0 relative z-10">
                    {children}
                </div>
                {!isSignIn && <Footer/>}
            </main>
        </div>
    </div>

}

export default Layout;