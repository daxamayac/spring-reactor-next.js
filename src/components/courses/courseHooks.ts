import {useMutation, useQueryClient} from  "@tanstack/react-query";
import {createCourse, deleteCourse, updateCourse} from "./services";
import _ from "../../@lodash";
import {closeDialog} from "../../store/slices/dialogSlice";
import {useSnackbar} from "notistack";
import {useDispatch} from "react-redux";

function useCRUD() {
    const queryClient = useQueryClient();
    const {enqueueSnackbar} = useSnackbar();
    const dispatch = useDispatch();

    const deleteCourseMutation = useMutation(deleteCourse, {
        onSuccess: (_data, variables) => {
            queryClient.setQueryData(["getCourses"], prevData => {
                // @ts-ignore
                return _.remove(prevData, function (item) {
                    // @ts-ignore
                    return item.id !== variables
                });
            })
            enqueueSnackbar("Course Deleted", {
                variant: 'success',
            });
            dispatch(closeDialog())
        }
    })

    const createCourseMutation = useMutation(createCourse, {
        onSuccess: data => {
            queryClient.setQueryData(["getCourses"], prevData => {
                // @ts-ignore
                return [data, ...prevData]
            })
            enqueueSnackbar("Course Created", {
                variant: 'success',
            });
            dispatch(closeDialog())
        }
    });

    const editCourseMutation = useMutation(updateCourse, {
        onSuccess: (data, variables) => {
            queryClient.setQueryData(["getCourses"], prevData => {
                // @ts-ignore
                return _.map(prevData, function (item) {
                    // @ts-ignore
                    return item.id === variables.id ? data : item;
                });
            })
            enqueueSnackbar("Course Updated", {
                variant: 'success',
            });
            dispatch(closeDialog())
        }
    });

    return { createCourseMutation,  deleteCourseMutation, editCourseMutation}
}

const courseHooks = {
    useCRUD
}
export default courseHooks;