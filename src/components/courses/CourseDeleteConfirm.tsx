import Button from '@mui/material/Button';
import ConfirmDialog from "../../shared-components/DeleteConfirmDialog";
import courseHooks from "./courseHooks";

// @ts-ignore
function CourseDeleteConfirm({title = "", detail = "", id}) {
    const {deleteCourseMutation} = courseHooks.useCRUD();

    return (
        <ConfirmDialog title={title} detail={detail}>
            <Button
                variant="contained"
                color="error"
                className=" w-full mt-16"
                aria-label="Delete Confirm"
                onClick={() => deleteCourseMutation.mutate(id)}
                type="button"
                size="large"
                data-role="button-form"
                disabled={deleteCourseMutation.isLoading}
            >
                Confirm
            </Button>

        </ConfirmDialog>
    );
}

export default CourseDeleteConfirm;
