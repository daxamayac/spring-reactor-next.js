// @ts-nocheck
import Button from '@mui/material/Button';
import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import Form from "../../shared-components/form/Form";
import Input from "../../shared-components/form/Input";
import {DialogContent, DialogTitle} from "@mui/material";
import {closeDialog} from "../../store/slices/dialogSlice";
import {useDispatch} from "react-redux";
import studentHooks from "./courseHooks";

const schema = yup.object().shape({
    name: yup
        .string()
        .required('Please enter the course name.')
        .max(30, 'Name is too big - must be less than 20 characters.'),
    acronym: yup
        .string()
        .required('Please enter the acronym.')
        .max(20, 'Name is too big - must be less than 20 characters.'),
    status: yup
        .boolean()
        .required('Please enter the status.')
});
const initialValues = {
    name: '',
    acronym: '',
    status: false
};

const defaultValues = {
    mode: 'onChange',
    defaultValues: initialValues,
    resolver: yupResolver(schema),
};

function CourseForm({errors = null, course}) {
    const dispatch = useDispatch();
    const {createCourseMutation, editCourseMutation} = studentHooks.useCRUD();
    const isNew = course === "new";
    const titleLabel = isNew ? "New" : "Edit";

    function onSubmit(data) {
        if (isNew)
            createCourseMutation.mutate(data)
        else
            editCourseMutation.mutate(data)
    }

    return (
        <>
            <DialogTitle id="alert-dialog-title">{`${titleLabel} Course`} </DialogTitle>
            <DialogContent>
                <Form
                    name="courseForm"
                    noValidate
                    className="flex flex-col justify-center w-full sm:min-w-xs mt-32"
                    onSubmit={onSubmit}
                    defaultValues={defaultValues}
                    data={course}
                    autoComplete={"off"}
                    _errors={errors}
                >
                    <Input.Text name="name" label="Name" type="text" required/>

                    <Input.Text name="acronym" label="Acronym" type="text" required/>

                    <Input.Check name="status" label="Status" required/>

                    <div className="flex flex-row  space-x-10 justify-center w-full sm:min-w-xs"
                         data-role="input-container">
                        <Button
                            onClick={() => dispatch(closeDialog())}
                            variant="contained"
                            className=" w-full mt-16"
                            aria-label="Sign in"
                            size="large"
                            data-role="button-form"
                        >
                            Cancel
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            className=" w-full mt-16"
                            aria-label="Sign in"
                            type="submit"
                            size="large"
                            data-role="button-form"
                            isLoading={createCourseMutation.isLoading || editCourseMutation.isLoading}
                        >
                            Save
                        </Button>
                    </div>
                </Form>
            </DialogContent>
        </>
    );
}

export default CourseForm;
