import axios from 'axios';
import {Course} from "./Course";

const COURSE_ENDPOINT = "/v2/courses"

export function getCourses(_params?:any) {
    return axios.get(COURSE_ENDPOINT,{params:_params}).then((resp) => resp.data as Course[]);
}

export function createCourse(data: Course) {
    return axios.post(COURSE_ENDPOINT, data).then((resp) => resp.data);
}

export function deleteCourse(id: string | number) {
    return axios.delete(COURSE_ENDPOINT + "/" + id).then((resp) => resp.data);
}

export function updateCourse(data: Course) {
    return axios.put(COURSE_ENDPOINT + "/" + data.id, data).then((resp) => resp.data);

}
