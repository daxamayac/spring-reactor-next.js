import {ListItemComponent} from "../../components/CustomList/CustomList";
import {Avatar, Icon, IconButton, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText} from "@mui/material";
import {Delete, Edit} from "@mui/icons-material";
import {Course} from "./Course";

const CourseListItem: React.FC<ListItemComponent<Course>> = ({item, onUpdate, onDelete}) => {
    return <ListItem key={item.id} divider>
        <ListItemAvatar>
            <Avatar>
                <Icon>school</Icon>
            </Avatar>
        </ListItemAvatar>
        <ListItemText
            primary={<>
                <strong>{item.acronym}</strong>
                <div>{item.name}</div>
            </>}
            secondary={`Status: ${item.status}`}
        />

        <ListItemSecondaryAction>
            <IconButton onClick={() => onUpdate(item)} className="text-blue-800" aria-label={"Edit"}>
                <Edit />
            </IconButton>
            <IconButton onClick={() => onDelete(item)} className="text-red-800" aria-label={"Delete"}>
                <Delete/>
            </IconButton>
        </ListItemSecondaryAction>

    </ListItem>
}
export default CourseListItem