import Dialog from '@mui/material/Dialog';

import {closeDialog, selectDialogOptions, selectDialogState} from "../../store/slices/dialogSlice";
import {useAppDispatch, useAppSelector} from "../../store/hooks";

function DaxDialog() {
  const dispatch = useAppDispatch();
  const state = useAppSelector(selectDialogState);
  const options = useAppSelector(selectDialogOptions);
const {children, ...props} = options;
  return (
    <Dialog
      open={state}
      onClose={(ev) => dispatch(closeDialog())}
      aria-labelledby="dialog-title"
      classes={{
        paper: 'rounded-8',
      }}
      {...props}
    >
        {children}
    </Dialog>
  );
}

export default DaxDialog;
