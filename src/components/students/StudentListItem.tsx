import {ListItemComponent} from "../../components/CustomList/CustomList";
import {Avatar, Icon, IconButton, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText} from "@mui/material";
import {Delete, Edit} from "@mui/icons-material";
import {Student} from "./Student";
import React from "react";

const StudentListItem: React.FC<ListItemComponent<Student>> = ({item, onUpdate, onDelete}) => {
    return <ListItem key={item.id} divider>
        <ListItemAvatar>
            <Avatar>
                <Icon color="primary">person</Icon>
            </Avatar>
        </ListItemAvatar>
        <ListItemText
            primary={<>
                <strong>{item.dni}</strong>
                <div>{item.name +" "+ item.lastName}</div>
            </>}
            secondary={`Age: ${item.age}`}
        />

        <ListItemSecondaryAction>
            <IconButton onClick={() => onUpdate(item)} className="text-blue-800" aria-label="Edit">
                <Edit/>
            </IconButton>
            <IconButton onClick={() => onDelete(item)} className="text-red-800" aria-label="Delete">
                <Delete/>
            </IconButton>
        </ListItemSecondaryAction>

    </ListItem>
}
export default StudentListItem