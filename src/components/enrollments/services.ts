import axios from 'axios';

const ENROLLMENT_ENDPOINT = "/v2/enrollments"

export function getEnrollments() {
    return axios.get(ENROLLMENT_ENDPOINT).then((resp) => resp.data);
}

export function getEnrollmentsByStudent(_params: any) {
    const {id} = _params

    return axios.get(ENROLLMENT_ENDPOINT + "/students/" + id).then((resp) => resp.data);
}

export function createEnrollment(data: any) {
    return axios.post(ENROLLMENT_ENDPOINT, data).then((resp) => resp.data);
}

export function updateEnrollment(data: any) {
    return axios.put(ENROLLMENT_ENDPOINT + "/" + data.id, data).then((resp) => resp.data);

}