import {useMutation} from  "@tanstack/react-query";
import {createEnrollment, updateEnrollment} from "./services";
import {closeDialog} from "../../store/slices/dialogSlice";
import {useSnackbar} from "notistack";
import {useDispatch} from "react-redux";

function useCRUD() {
    const {enqueueSnackbar} = useSnackbar();
    const dispatch = useDispatch();

    const createEnrollmentMutation = useMutation(createEnrollment, {
        onSuccess: data => {
            enqueueSnackbar("Enrollment Created", {
                variant: 'success',
            });
            dispatch(closeDialog())
        }
    });

    const editEnrollmentMutation = useMutation(updateEnrollment, {
        onSuccess: (data, variables) => {

            enqueueSnackbar("Enrollment Updated", {
                variant: 'success',
            });
            dispatch(closeDialog())
        }
    });

    return {createEnrollmentMutation,editEnrollmentMutation}
}

const enrollmentHook = {
    useCRUD
}

export default enrollmentHook;