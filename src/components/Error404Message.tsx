import Box from '@mui/material/Box';
import Link from "next/link";

function Error404Message() {
    return (
        <Box
            className="relative md:flex flex-auto items-center justify-center h-full p-64 lg:px-112 overflow-hidden"
            sx={{backgroundColor: 'primary.main'}}
        >
            <div className="z-10 relative w-full max-w-2xl">
                <div className="text-7xl font-bold leading-none text-gray-100">
                    <div>Lo sentimos no hemos encontrado la página</div>
                    <Link  href="/students" >
                      <span className="block font-normal mt-48 cursor-pointer underline text-blue-400 hover:text-light-blue-300 text-4xl w-fit">Back to Students</span>
                    </Link>
                </div>
                <div className="mt-24 text-lg tracking-tight leading-6 text-gray-400">
                   Error 404
                </div>
                <div className="flex items-center mt-32">
                    <div className=" font-medium tracking-tight text-gray-400">
                        By @daxamayac
                    </div>
                </div>
            </div>
        </Box>
    );
}

export default Error404Message;
