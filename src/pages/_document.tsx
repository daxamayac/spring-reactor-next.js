import {Head, Html, Main, NextScript} from "next/document";

function Document() {
    return <Html lang="en">
        <Head>
            <meta name="description" content="Next js"/>
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons&display=swap" rel="stylesheet" />
        </Head>
        <body>
        <Main/>
        <NextScript/>
        </body>

    </Html>
}

export default Document
