import Typography from '@mui/material/Typography';
import CourseForm from '../components/courses/CourseForm';
import Button from "@mui/material/Button";
import {useDispatch} from "react-redux";
import {openDialog} from "../store/slices/dialogSlice";
import {useQuery} from "@tanstack/react-query";
import {getCourses} from "../components/courses/services";
import {CircularProgress} from "@mui/material";
import CustomList from "../components/CustomList";
import _ from "../@lodash";
import {Course} from "../components/courses/Course";
import CourseListItem from "../components/courses/CourseListItem";
import CourseDeleteConfirm from "../components/courses/CourseDeleteConfirm";
import Head from "next/head";

function CoursePage() {
    const dispatch = useDispatch();
    const {isLoading, data: courses} = useQuery(["getCourses"], () => getCourses(), {
        retry: false,
        refetchOnWindowFocus: false
    })

    function handleDelete(item: Course) {
        dispatch(openDialog({
            children: <CourseDeleteConfirm title="Delete Course" detail={item?.name} id={item.id}/>
        }))
    }

    function handleUpdate(item: string | Course) {
        dispatch(openDialog({
            children: <CourseForm course={item}/>
        }))
    }

    return (
        <>
            <Head><title>Students</title></Head>
            <div className="flex flex-col flex-auto items-center mt-48 min-w-0 md:p-32">
                <Typography variant="h3">Courses</Typography>
                <div className="w-8/12 min-h-sm">
                    <Button
                        onClick={() => dispatch(openDialog({
                            children: <CourseForm course="new"/>
                        }))}
                        variant="contained"
                        color="primary"
                        className=" w-full max-w-xs mt-16"
                        aria-label="Sign in"
                        size="large"
                        data-role="button-form"
                    >
                        Create new course
                    </Button>
                    <div className="m-32">
                        {isLoading && <CircularProgress/>}
                        {courses &&
                            <CustomList<Course> collection={courses} onUpdate={handleUpdate} onDelete={handleDelete}
                                                renderAs={CourseListItem}/>}
                        {!isLoading && _.isEmpty(courses) && "No data"}
                    </div>
                </div>
            </div>
        </>
    );
}

export default CoursePage;
