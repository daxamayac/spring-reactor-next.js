import Typography from '@mui/material/Typography';
import StudentForm from '../components/students/StudentForm';
import Button from "@mui/material/Button";
import {useDispatch} from "react-redux";
import {openDialog} from "../store/slices/dialogSlice";
import {useQuery} from "@tanstack/react-query";
import {getStudents} from "../components/students/services";
import {CircularProgress} from "@mui/material";
import CustomList from "../components/CustomList";
import StudentListItem from "../components/students/StudentListItem";
import _ from "../@lodash";
import {Student} from "../components/students/Student";
import StudentDeleteConfirm from "../components/students/StudentDeleteConfirm";
import Head from "next/head";

function Students() {
    const dispatch = useDispatch();
    const {isLoading, data: students} = useQuery(["getStudents"], getStudents, {
        retry: false,
        refetchOnWindowFocus: false
    })

    function handleDelete(item: Student) {
        dispatch(openDialog({
            children: <StudentDeleteConfirm title="Delete Student" detail={item?.name + " " + item?.lastName} id={item.id}/>
        }))
    }

    function handleUpdate(item: string | Student) {
        dispatch(openDialog({
            children: <StudentForm student={item}/>
        }))
    }

    return (<>
            <Head><title>Students</title></Head>
        <div className="flex flex-col flex-auto items-center mt-48 min-w-0 md:p-32">
            <Typography variant="h3">Students</Typography>
            <div className="w-8/12 min-h-sm">
                <Button
                    onClick={() => dispatch(openDialog({
                        children: <StudentForm student="new"/>
                    }))}
                    variant="contained"
                    color="primary"
                    className=" w-full max-w-xs mt-16"
                    aria-label="Sign in"
                    size="large"
                    data-role="button-form"
                >
                    Create new student
                </Button>
                <div className="m-32">
                    {isLoading && <CircularProgress/>}
                    {students &&
                        <CustomList<Student> collection={students} onUpdate={handleUpdate} onDelete={handleDelete}
                                             renderAs={StudentListItem}/>}
                    {!isLoading && _.isEmpty(students) && "No data"}
                </div>
            </div>
        </div>
        </>
    );
}

export default Students;
