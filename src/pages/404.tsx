import Paper from '@mui/material/Paper';

import Error404Message from '../components/Error404Message';

function Error404Page() {

    return (
        <div className="flex flex-col flex-auto items-center sm:justify-center min-w-0 md:p-32"
             style={{
                 backgroundImage: 'url(https://source.unsplash.com/random)',
                 backgroundRepeat: 'no-repeat',
                 backgroundSize: 'cover',
                 backgroundPosition: 'center',
             }}
        >
            <Paper
                className="flex w-full sm:w-auto min-h-full sm:min-h-auto md:w-full md:max-w-6xl rounded-0 sm:rounded-2xl sm:shadow overflow-hidden">
                <Error404Message/>
            </Paper>
        </div>
    );
}

export default Error404Page;
