import '../styles/globals.css'
import type {AppProps} from 'next/app'
import Layout from "../components/layout/Layout";
import {Provider} from "react-redux";
import {store} from "../store/store";
import {createTheme, StyledEngineProvider, ThemeProvider} from "@mui/material";
import {themeOptions} from "../configs/themeConfig";
import {
    QueryClient,
    QueryClientProvider,
} from '@tanstack/react-query'
import axios from "axios";
import AuthProvider from "../auth/AuthProvider";
import CookieConsent from "../shared-components/CookieConsent";
import {SnackbarProvider} from "notistack";
import Head from "next/head";

axios.defaults.baseURL = process.env.NEXT_PUBLIC_API_BASEPATH;

const queryClient = new QueryClient();

function MyApp({Component, pageProps}: AppProps) {
    return<>
        <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

        </Head>
     <QueryClientProvider client={queryClient}>
        <AuthProvider>
            <StyledEngineProvider injectFirst>
                <ThemeProvider theme={createTheme(themeOptions)}>
                    <Provider store={store}>
                        <AuthProvider>
                            <SnackbarProvider
                                maxSnack={3}
                                autoHideDuration={3000}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}
                                classes={{
                                    containerRoot: 'bottom-0 right-0 mb-52 md:mb-68 mr-8 lg:mr-80 z-99',
                                }}
                            >
                                <Layout>
                                    <CookieConsent onAccept={() => console.log("Thanks by agree. Att: @daxamayac")}/>
                                    <Component {...pageProps} />
                                </Layout>
                            </SnackbarProvider>
                        </AuthProvider>
                    </Provider>
                </ThemeProvider>
            </StyledEngineProvider>
        </AuthProvider>
    </QueryClientProvider>
    </>
}

export default MyApp
