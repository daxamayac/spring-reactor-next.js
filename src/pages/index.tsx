import {useRouter} from "next/router";

const Home = () => {
    const router = useRouter();
    router.push("/sign-in");
    return <>as</>
}

export default Home
