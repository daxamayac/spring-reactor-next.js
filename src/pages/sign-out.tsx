import {useEffect} from 'react';
import {signOut} from "../auth/authServices";
import {useRouter} from "next/router";
import Head from "next/head";

function SignOutPage() {
    const router = useRouter();
    useEffect(() => {
            signOut();
            router.push("sign-in");
        }
    )

    return (
        <>
            <Head><title>Sign out</title></Head>
            Cerrando Sesión</>
    );
}

export default SignOutPage;
