import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import {useEffect, useState} from 'react';
import SignInForm from '../components/auth/SignInForm';
import SignInWelcome from '../components/auth/SignInWelcome';
import {useMutation} from "@tanstack/react-query";
import {setSession, signIn} from "../auth/authServices";
import {Alert} from "@mui/material";
import {useRouter} from "next/router";
import {NextPage} from "next";
import Head from "next/head";

interface signInProps {
    email: string,
    password: string
}

const SignIn: NextPage =()=>{
    const router = useRouter();
    const [error, setError] = useState("");
    const _signIn = useMutation(signIn);

    useEffect(() => {
        router.prefetch('/students')
    }, [])


    function onSubmit({email, password}: signInProps) {
        setError("");
        _signIn.mutateAsync({username: email, password})
            .then((resp) => {
                    setSession(resp.token);
                    router.push("/students", undefined, {shallow: true});
                }
            )
            .catch((_errors) => {
                setError("Credenciales incorrectas");
            });
        ;
    }

    return (<>
            <Head><title>Sign In</title></Head>
            <div className="flex flex-col flex-auto items-center sm:justify-center min-w-0 md:p-32"
                 style={{
                     backgroundImage: 'url(https://source.unsplash.com/random)',
                     backgroundRepeat: 'no-repeat',
                     backgroundSize: 'cover',
                     backgroundPosition: 'center',
                 }}
            >
                <Paper
                    className="flex w-full  opacity-90 sm:w-auto min-h-full sm:min-h-auto md:w-full md:max-w-6xl rounded-0 sm:rounded-2xl sm:shadow overflow-hidden">
                    <div className="w-full sm:w-auto py-32 px-16 sm:p-48 md:p-64 ltr:border-r-1 rtl:border-l-1">
                        <div className="w-full max-w-320 sm:w-320 mx-auto sm:mx-0">

                            <Typography className="mt-32 text-4xl font-extrabold tracking-tight leading-tight">
                                Sign in
                            </Typography>
                            {error &&
                                <Alert variant="filled" severity="error" className="mt-32">
                                    {error}
                                </Alert>
                            }

                            <SignInForm className="flex flex-col justify-center w-full mt-32" onSubmit={onSubmit}
                                        isLoading={_signIn.isLoading} errors={undefined}/>
                        </div>
                    </div>

                    <SignInWelcome/>
                </Paper>
            </div>
        </>
    );
}

export default SignIn;
