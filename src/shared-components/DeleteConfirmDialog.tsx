import {DialogContent, DialogTitle} from "@mui/material";
import Button from "@mui/material/Button";
import {closeDialog} from "../store/slices/dialogSlice";
import {useDispatch} from "react-redux";

function DeleteConfirmDialog({
                           title = "", detail = "", children = <></>}) {
    const dispatch = useDispatch();
    return <>
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        <DialogContent>
            <span>Are you sure you want to delete <strong>{detail}</strong> ?</span>
            <div className="flex flex-row  space-x-10 justify-center w-full sm:min-w-xs mt-32">
                <Button
                    onClick={() => dispatch(closeDialog())}
                    variant="contained"
                    className=" w-full mt-16"
                    aria-label="Sign in"
                    size="large"
                    data-role="button-form"
                >
                    Cancel
                </Button>
                {children}
            </div>
        </DialogContent>
    </>
}

export default DeleteConfirmDialog