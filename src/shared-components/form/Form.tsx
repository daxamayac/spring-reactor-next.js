import React, {useEffect} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {generateGUID} from "../../@utils/@utils";
import _ from "../../@lodash";

function renderInput(children: any, control: any, dirtyFields?: any, isValid?: any, errors?: any): any {
    return Array.isArray(children)
        ? children.map((child) => {

            if (child.props['data-role'] === 'input-container') {
                return React.createElement(
                    child.type,
                    {
                        className: child.props.className,
                        key: child.key || generateGUID(),
                    },
                    renderInput(child.props.children, control,dirtyFields ,isValid, errors)
                );
            }

            if (child.props['data-role'] === 'button-form') {
                const {isLoading, ...props} = child.props;
                return React.createElement(child.type, {
                    key: child.key || generateGUID(),
                    disabled: _.isEmpty(dirtyFields) || !isValid || isLoading,
                    ...props,
                });
            }

            return child.props.name ? (
                <Controller
                    name={child.props.name}
                    key={child.props.name}
                    control={control}
                    render={({field}) =>
                        React.cloneElement(child, {field, errors: errors?.[child.props.name] ?? null})
                    }
                />
            ) : (
                child
            );
        })
        : children;
}

// @ts-ignore
function Form({defaultValues, children, onSubmit, data, _errors, ...props}) {
    const {control, formState, handleSubmit, setError, reset} = useForm({...defaultValues});
    const {isValid, dirtyFields, errors} = formState;

    useEffect(() => {
        if (_errors) {
            _errors.forEach((error: any) => {
                setError(error.name, {
                    type: error.type,
                    message: error.message,
                });
            });
        }
    }, [_errors, setError]);

    useEffect(() => {
        if (data !== "new")
            reset(data)
    }, [data, reset]);

    return (
        <form {...props} onSubmit={handleSubmit(onSubmit)}>
            {renderInput(children, control, dirtyFields, isValid, errors)}
        </form>
    );
}

export default Form;
