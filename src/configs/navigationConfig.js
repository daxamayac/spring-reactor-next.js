const navigationConfig = [
    {
        id: 'students',
        title: 'Students',
        url: '/students',
    },
    {
        id: 'courses',
        title: 'Courses',
        url: '/courses',
    },
    {
        id: 'enrollments',
        title: 'Enrollments',
        url: '/enrollments',
    },
    {
        id: 'signOut',
        title: 'Sign out',
        url: '/sign-out',
    },
];

export default navigationConfig;
