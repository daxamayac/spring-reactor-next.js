import {ThemeOptions} from "@mui/material";
import _ from "../@lodash";

const lightPaletteText = {
    primary: 'rgb(17, 24, 39)',
    secondary: 'rgb(107, 114, 128)',
    disabled: 'rgb(149, 156, 169)',
};

const darkPaletteText = {
    primary: 'rgb(255,255,255)',
    secondary: 'rgb(148, 163, 184)',
    disabled: 'rgb(156, 163, 175)',
};
const skyBlue = {
    50: '#e4fafd',
    100: '#bdf2fa',
    200: '#91e9f7',
    300: '#64e0f3',
    400: '#43daf1',
    500: '#22d3ee',
    600: '#1eceec',
    700: '#19c8e9',
    800: '#14c2e7',
    900: '#0cb7e2',
    A100: '#ffffff',
    A200: '#daf7ff',
    A400: '#a7ecff',
    A700: '#8de6ff',
    contrastDefaultColor: 'dark',
};
const dark = {
    50: '#e5e6e8',
    100: '#bec1c5',
    200: '#92979f',
    300: '#666d78',
    400: '#464e5b',
    500: '#252f3e',
    600: '#212a38',
    700: '#1b2330',
    800: '#161d28',
    900: '#0d121b',
    A100: '#5d8eff',
    A200: '#2a6aff',
    A400: '#004af6',
    A700: '#0042dd',
    contrastDefaultColor: 'light',
};
const theme: ThemeOptions = {

    palette: {
        mode: 'light',
        divider: '#e2e8f0',
        text: lightPaletteText,
        common: {
            black: 'rgb(17, 24, 39)',
            white: 'rgb(255, 255, 255)',
        },
        primary: {
            light: dark[200],
            main: dark[500],
            dark: dark[900],
            contrastText: darkPaletteText.primary,
        },
        secondary: {
            light: skyBlue[100],
            main: skyBlue[500],
            dark: skyBlue[900],
            contrastText: lightPaletteText.primary,
        },
        background: {
            paper: '#FFFFFF',
            default: '#f6f7f9',
        },
        error: {
            light: '#ffcdd2',
            main: '#f44336',
            dark: '#b71c1c',
        },

    }
};
const defaultThemeOptions: ThemeOptions = {
    typography: {
        fontFamily: ['Inter var', 'Roboto', '"Helvetica"', 'Arial', 'sans-serif'].join(','),
        fontWeightLight: 300,
        fontWeightRegular: 400,
        fontWeightMedium: 500,
    },
    components: {
        MuiAppBar: {
            defaultProps: {
                enableColorOnDark: true,
            },
            styleOverrides: {
                root: {
                    backgroundImage: 'none',
                },
            },
        },
        MuiButtonBase: {
            defaultProps: {
                disableRipple: false,
            },
        },
        MuiButton: {
            defaultProps: {
                variant: 'text',
                color: 'inherit',
            },
            styleOverrides: {
                root: {
                    textTransform: 'none',
                    // lineHeight: 1,
                },
                sizeMedium: {
                    borderRadius: 20,
                    height: 40,
                    minHeight: 40,
                    maxHeight: 40,
                },
                sizeSmall: {
                    borderRadius: '15px',
                },
                sizeLarge: {
                    borderRadius: '28px',
                },
                contained: {
                    boxShadow: 'none',
                    '&:hover, &:focus': {
                        boxShadow: 'none',
                    },
                },
            },
        },
        MuiButtonGroup: {
            defaultProps: {
                color: 'secondary',
            },
            styleOverrides: {
                contained: {
                    borderRadius: 18,
                },
            },
        },
        MuiTab: {
            styleOverrides: {
                root: {
                    textTransform: 'none',
                },
            },
        },
        MuiDialog: {
            styleOverrides: {
                paper: {
                    borderRadius: 16,
                },
            },
        },
        MuiPaper: {
            styleOverrides: {
                root: {
                    backgroundImage: 'none',
                },
                rounded: {
                    borderRadius: 16,
                },
            },
        },
        MuiPopover: {
            styleOverrides: {
                paper: {
                    borderRadius: 8,
                },
            },
        },
        MuiTextField: {
            defaultProps: {
                color: 'secondary',
            },
        },
        MuiInputLabel: {
            defaultProps: {
                color: 'secondary',
            },
        },
        MuiSelect: {
            defaultProps: {
                color: 'secondary',
            },
        },
        MuiOutlinedInput: {
            defaultProps: {
                color: 'secondary',
            },
        },
        MuiInputBase: {
            styleOverrides: {
                root: {
                    minHeight: 40,
                    lineHeight: 1,
                },
            },
        },
        MuiFilledInput: {
            styleOverrides: {
                root: {
                    borderRadius: 4,
                    '&:before, &:after': {
                        display: 'none',
                    },
                },
            },
        },
        MuiSlider: {
            defaultProps: {
                color: 'secondary',
            },
        },
        MuiCheckbox: {
            defaultProps: {
                color: 'secondary',
            },
        },
        MuiRadio: {
            defaultProps: {
                color: 'secondary',
            },
        },
        MuiSwitch: {
            defaultProps: {
                color: 'secondary',
            },
        },
        MuiTypography: {
            variants: [
                {
                    props: {color: 'text.secondary'},
                    style: {
                        color: 'text.secondary',
                    },
                },
            ],
        },
    },
};
const mustHaveThemeOptions: ThemeOptions = {
    typography: {
        htmlFontSize: 10,
        fontSize: 14,
        body1: {
            fontSize: '1.4rem',
        },
        body2: {
            fontSize: '1.4rem',
        },
    },
};

export const themeOptions: ThemeOptions = _.merge({}, defaultThemeOptions, theme, mustHaveThemeOptions);